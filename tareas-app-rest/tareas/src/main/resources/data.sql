insert into tareas (id, descripcion, fecha_creacion, is_valido)
values (1,'Tarea de prueba 1','2021-02-19T01:34:08.057Z',true);

insert into tareas (id, descripcion, fecha_creacion, is_valido)
values (2,'Tarea de prueba 2','2021-02-19T01:34:08.057Z',true);

insert into tareas (id, descripcion, fecha_creacion, is_valido)
values (3,'Tarea de prueba 3','2021-02-19T01:34:08.057Z',true);

insert into tareas (id, descripcion, fecha_creacion, is_valido)
values (4,'Tarea de prueba 4','2021-02-19T01:34:08.057Z',false);

insert into tareas (id, descripcion, fecha_creacion, is_valido)
values (5,'Tarea de prueba 5','2021-02-19T01:34:08.057Z',true);