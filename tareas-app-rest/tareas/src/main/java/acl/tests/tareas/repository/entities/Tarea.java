package acl.tests.tareas.repository.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "tareas")
public class Tarea {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String descripcion;
    private Boolean isValido;
    @Column(columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaCreacion;
}
