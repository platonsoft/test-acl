package acl.tests.tareas.configuration;

public class TareasExceptionRuntime extends RuntimeException{
    public TareasExceptionRuntime(String s) {
        super(s);
    }
}
