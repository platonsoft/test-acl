package acl.tests.tareas.services;

import acl.tests.tareas.repository.model.ResponseTarea;
import acl.tests.tareas.repository.model.TareaDto;
import org.springframework.http.ResponseEntity;

public interface ITareaService {
    String MENSAJE_EXITOSO = "Su solicitud fue procesada exitosamente";
    String MENSAJE_FALLIDO = "Ocurrio un error inesperado, Contacte con el administrador del sistema";
    String MENSAJE_FALTA_DESCRIPCION = "la descripcion es requerida";

    ResponseEntity<ResponseTarea> listaTareas();
    ResponseEntity<ResponseTarea> addTarea(TareaDto tarea);
    ResponseEntity<ResponseTarea> editTarea(TareaDto tarea);
    ResponseEntity<ResponseTarea> deleteTarea(Long idTarea);

    enum MensajesUsuario{
        MENSAJES_USUARIO_EXITOSO(0L, ITareaService.MENSAJE_EXITOSO),
        MENSAJES_USUARIO_FALLIDO(-1L, ITareaService.MENSAJE_FALLIDO);

        private final Long codigo;
        private final String mensaje;

        MensajesUsuario(Long codigo, String mensaje) {
            this.codigo = codigo;
            this.mensaje = mensaje;
        }

        public Long getCodigo() {
            return codigo;
        }

        public String getMensaje() {
            return mensaje;
        }
    }
}
