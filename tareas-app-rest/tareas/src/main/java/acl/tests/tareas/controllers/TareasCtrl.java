package acl.tests.tareas.controllers;

import acl.tests.tareas.repository.model.ResponseTarea;
import acl.tests.tareas.repository.model.TareaDto;
import acl.tests.tareas.services.ITareaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = {"Tareas"})
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RequestMapping("api")
public class TareasCtrl {

    private final ITareaService iTareaService;

    public TareasCtrl(ITareaService iTareaService) {
        this.iTareaService = iTareaService;
    }

    @GetMapping(value = "/tareas")
    @ApiOperation(value = "Listado de tareas", notes = "Muestra la lista de tareas registradas")
    public ResponseEntity<ResponseTarea> listaTareas(){
        return iTareaService.listaTareas();
    }

    @PostMapping(value = "/tarea")
    @ApiOperation(value = "Tarea Nueva", notes = "Agrega una tarea Nueva")
    public ResponseEntity<ResponseTarea> agregarTareas(@RequestBody TareaDto tarea){
        return iTareaService.addTarea(tarea);
    }

    @PutMapping(value = "/tarea")
    @ApiOperation(value = "Actualizar tarea", notes = "Actualiza una tarea")
    public ResponseEntity<ResponseTarea> actualizarTareas(@RequestBody TareaDto tarea){
        return iTareaService.editTarea(tarea);
    }

    @DeleteMapping(value = "/tarea/{idTarea}")
    @ApiOperation(value = "Borrar tarea", notes = "Borrar una tarea")
    public ResponseEntity<ResponseTarea> eliminarTareas(@PathVariable Long idTarea){
        return iTareaService.deleteTarea(idTarea);
    }
}
