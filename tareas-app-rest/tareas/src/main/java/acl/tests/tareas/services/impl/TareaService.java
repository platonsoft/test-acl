package acl.tests.tareas.services.impl;

import acl.tests.tareas.configuration.TareasExceptionRuntime;
import acl.tests.tareas.repository.daos.ITareaDao;
import acl.tests.tareas.repository.entities.Tarea;
import acl.tests.tareas.repository.model.ResponseTarea;
import acl.tests.tareas.repository.model.TareaDto;
import acl.tests.tareas.services.ITareaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;

@Service
public class TareaService implements ITareaService {

    private final ITareaDao iTareaDao;

    public TareaService(ITareaDao iTareaDao) {
        this.iTareaDao = iTareaDao;
    }

    @Override
    public ResponseEntity<ResponseTarea> listaTareas() {
        try {
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getMensaje())
                    .data(iTareaDao.findAll())
                    .build(), HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getMensaje())
                    .data(Collections.emptyList())
                    .build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<ResponseTarea> addTarea(TareaDto tarea) {
        try {
            if (tarea.getDescripcion()==null){
                throw new TareasExceptionRuntime(MENSAJE_FALTA_DESCRIPCION);
            }
            Tarea tareaNew = new Tarea();
            tareaNew.setIsValido(tarea.getIsValido());
            tareaNew.setDescripcion(tarea.getDescripcion());
            tareaNew.setFechaCreacion(new Date());

            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getMensaje())
                    .data(iTareaDao.save(tareaNew))
                    .build(), HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getMensaje())
                    .data(Collections.emptyList())
                    .build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<ResponseTarea> editTarea(TareaDto tarea) {
        try {
            Tarea tareaEdit = iTareaDao.getOne(tarea.getId());
            tareaEdit.setDescripcion(tarea.getDescripcion());
            tareaEdit.setIsValido(tarea.getIsValido());

            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getMensaje())
                    .data(iTareaDao.save(tareaEdit))
                    .build(), HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getMensaje())
                    .data(Collections.emptyList())
                    .build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<ResponseTarea> deleteTarea(Long idTarea) {
        try {
            iTareaDao.deleteById(idTarea);
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_EXITOSO.getMensaje())
                    .data(Collections.emptyList())
                    .build(), HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(ResponseTarea.builder()
                    .cod(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getCodigo())
                    .mensaje(MensajesUsuario.MENSAJES_USUARIO_FALLIDO.getMensaje())
                    .data(Collections.emptyList())
                    .build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
