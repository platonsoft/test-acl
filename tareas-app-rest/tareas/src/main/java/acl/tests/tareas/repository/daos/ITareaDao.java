package acl.tests.tareas.repository.daos;

import acl.tests.tareas.repository.entities.Tarea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITareaDao extends JpaRepository<Tarea, Long> {
}
