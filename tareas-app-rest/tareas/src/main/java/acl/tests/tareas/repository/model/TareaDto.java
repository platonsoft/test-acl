package acl.tests.tareas.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TareaDto {
    private Long id;
    private String descripcion;
    private Boolean isValido;
    private Date fechaCreacion;
}
