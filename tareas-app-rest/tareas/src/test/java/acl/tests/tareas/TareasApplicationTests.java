package acl.tests.tareas;

import acl.tests.tareas.controllers.TareasCtrl;
import acl.tests.tareas.repository.model.ResponseTarea;
import acl.tests.tareas.repository.model.TareaDto;
import acl.tests.tareas.services.ITareaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TareasCtrl.class)
class TareasApplicationTests {

    final ResponseTarea respuestaExitosa = ResponseTarea.builder()
            .cod(0L)
            .mensaje(ITareaService.MENSAJE_EXITOSO)
            .data(Collections.emptyList())
            .build();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ITareaService iTareaService;

    @TestConfiguration
    static class TestConfigurationApp {
        @Bean
        ObjectMapper objectMapperPrettyPrinting() {
            return new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    @Test
    void getTareas() throws Exception {
        ResponseEntity<ResponseTarea> response = new ResponseEntity<>(this.respuestaExitosa, HttpStatus.OK);
        given(iTareaService.listaTareas()).willReturn(response);

        mockMvc.perform(get("/api/tareas")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['cod']", is(this.respuestaExitosa.getCod().intValue())))
                .andExpect(jsonPath("$['mensaje']", is(this.respuestaExitosa.getMensaje())));
    }


    @Test
    void addTareas() throws Exception {
        Gson gson = new Gson();
        ResponseEntity<ResponseTarea> response = new ResponseEntity<>(this.respuestaExitosa, HttpStatus.OK);

        TareaDto tareaDto = TareaDto.builder()
                .descripcion("Otra Tarea para el test")
                .isValido(true)
                .build();

        given(iTareaService.addTarea(tareaDto)).willReturn(response);

        mockMvc.perform(post("/api/tarea")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tareaDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['cod']", is(this.respuestaExitosa.getCod().intValue())))
                .andExpect(jsonPath("$['mensaje']", is(this.respuestaExitosa.getMensaje())));

        verify(iTareaService, VerificationModeFactory.times(1)).addTarea(tareaDto);
        reset(iTareaService);
    }

    @Test
    void editTareas() throws Exception {
        Gson gson = new Gson();
        ResponseEntity<ResponseTarea> response = new ResponseEntity<>(this.respuestaExitosa, HttpStatus.OK);

        TareaDto tareaDto = TareaDto.builder()
                .id(3L)
                .descripcion("Otra Tarea para el test")
                .isValido(true)
                .build();

        given(iTareaService.editTarea(tareaDto)).willReturn(response);

        mockMvc.perform(put("/api/tarea")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tareaDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['cod']", is(this.respuestaExitosa.getCod().intValue())))
                .andExpect(jsonPath("$['mensaje']", is(this.respuestaExitosa.getMensaje())));

        verify(iTareaService, VerificationModeFactory.times(1)).editTarea(tareaDto);
        reset(iTareaService);
    }

    @Test
    void deleteTareas() throws Exception {
        Gson gson = new Gson();
        ResponseEntity<ResponseTarea> response = new ResponseEntity<>(this.respuestaExitosa, HttpStatus.OK);

        Long isTareaAEliminar = 3L;
        given(iTareaService.deleteTarea(isTareaAEliminar)).willReturn(response);

        mockMvc.perform(delete("/api/tarea/" + isTareaAEliminar))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['cod']", is(this.respuestaExitosa.getCod().intValue())))
                .andExpect(jsonPath("$['mensaje']", is(this.respuestaExitosa.getMensaje())));

        verify(iTareaService, VerificationModeFactory.times(1)).deleteTarea(isTareaAEliminar);
        reset(iTareaService);
    }
}
