# Test Acl

Prueba solicitada acl

## Descripcion

El proyecto esta dividido en 2:
- Front: realizado en React-Redux
- Back: Servicios REST realizado en JAVA bajo Spring-Boot, JPA, H2, SWAGGER

Se cargaron 5 datos de prueba iniciales en memoria.

Los servicios se exponen en el puerto 8092 (Back), 3000 (front)
